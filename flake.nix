{
  description = "Podcast Horder. Never gonna let you go. Well, never gonna let you're podcasts go.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    poetry2nix,
  }: let
    supportedSystem = {
      x86_64-linux = "x86_64-linux";
    };
  in
    flake-utils.lib.eachSystem [supportedSystem.x86_64-linux] (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix {inherit pkgs;}) mkPoetryApplication;
      in {
        packages = {
          podcast-horder = mkPoetryApplication {projectDir = self;};
          default = self.packages.${system}.podcast-horder;
        };
        devShells.default = pkgs.mkShell {
          inputsFrom = [self.packages.${system}.podcast-horder];
          packages = [pkgs.poetry];
        };
        checks = flake-utils.lib.eachSystem [supportedSystem.x86_64-linux] (pkgs:
          nixpkgs.lib.optionalAttrs pkgs.stdenv.isLinux {
            evalNixos = let
              nixos = modules:
                (nixpkgs.lib.nixosSystem {
                  inherit (pkgs) system;
                  modules =
                    [
                      {
                        fileSystems."/" = {
                          device = "LABEL=sys";
                        };

                        boot.loader.grub.enable = false;

                        system.stateVersion = "23.11";
                      }
                    ]
                    ++ modules;
                })
                .config
                .system
                .build
                .toplevel;
            in
              pkgs.runCommand "eval-nixos" {
                plainSystem = nixos [];
                systemWithModules = nixos [self.nixosModules.all];
                systemWithModulesFull = nixos [self.nixosModules.allFull];
              } ''
                echo plainSystem: $plainSystem
                echo systemWithModules: $systemWithModules
                echo systemWithModulesFull: $systemWithModulesFull

                echo "check: 'all' modules don't change system output"
                [[ "$plainSystem" == "$systemWithModules" ]] || exit 1

                touch $out
              '';
          });
      }
    )
    // {
      nixosModules = rec {
        default = podcast-horder;
        podcast-horder = {
          config,
          lib,
          pkgs,
          ...
        }:
          with lib; let
            cfg = config.services.podcast-horder;
          in {
            options.services.podcast-horder.enable = mkEnableOption "enable Podcast Horder";
            options.services.podcast-horder.dryRun = mkOption {
              type = types.bool;
              default = false;
              description = "Simulates the server downloading items, debugging purposes, without making changes. Good for validating settings.";
              example = true;
            };
            options.services.podcast-horder.onlyNew = mkOption {
              type = types.bool;
              default = false;
              description = "Only process new podcast entries.";
            };
            options.services.podcast-horder.deleteOld = mkOption {
              type = types.bool;
              default = false;
              description = ''
                Delete entries older than `dateFrom` option date.
              '';
            };
            options.services.podcast-horder.dateFrom = mkOption {
              type = types.str;
              default = "1970-01-01"; #unix epoch time.
              description = "From date. This is the date that you will start downloading podcast episodes.";
              example = "2021-01-01";
            };
            options.services.podcast-horder.rootDirectory = mkOption {
              type = types.path;
              description = "Directory for downloads.";
              example = "/home/<your user name here>/podcasts";
            };
            options.services.podcast-horder.template = mkOption {
              type = types.str;
              default = "{rootdir}/{podcast}/{title}{ext}";
              description = ''
                List available keywords for filename template:
                  {rootdir} = base directory for downloads
                  {podcast} = key part of podcast dict
                  {date} = item publish date (format: YYYY.MM.DD)
                  {isodate} = item publish date (format: YYYY-MM-DD)
                  {title} = item title
                  {year} = item publish year (format: YYYY)
                  {month} = item publish month (format: MM)
                  {day} = item publish day (format: DD)
                  {ext} = enclosure file extension
                  {guid} = GUID of the episode
              '';
              example = "{rootdir}/{podcast}/{year}/{year}-{month}-{day} - {title}{ext}";
            };
            options.services.podcast-horder.userAgent = mkOption {
              type = types.str;
              default = "Mozilla/5.0";
              description = "Some times, when a request loves a server, they don't get along, you can change this to mimic a browser... or something else.";
              example = "Mozilla/5.0";
            };
            options.services.podcast-horder.checkFrequency = mkOption {
              type = types.str;
              default = "6h";
              description = "the frequeceny you wish to check rss feeds for new episodes, default is every 6h";
              example = "5h";
            };
            options.services.podcast-horder.cronUser = mkOption {
              type = types.str;
              default = "root";
              description = "For the service's cronjob, what user should this run as? Default is root.";
              example = "root";
            };
            options.services.podcast-horder.podcastList = mkOption {
              type = types.listOf types.attrs;
              default = [];
              example = [
                {
                  url = "https://blart.libsyn.com/rss";
                  title = "Til Death Do Us Blart";
                }
                {
                  url = "https://feeds.simplecast.com/Urk3897_";
                  title = "The Besties";
                }
              ];
            };

            config = lib.mkIf cfg.enable {
              environment.systemPackages = [
                self.packages."x86_64-linux".podcast-horder
              ];

              # This is the default location the horder script checks for the config.
              environment.etc."/podcast-horder/config.json".text = let
                opts = {
                  run = true; # This is always true.
                  dryrun = cfg.dryRun;
                  onlyNew = cfg.onlyNew;
                  deleteold = cfg.deleteOld;
                  date_from = cfg.dateFrom;
                  root_dir = cfg.rootDirectory;
                  template = cfg.template;
                  user_agent = cfg.userAgent;
                };
              in
                builtins.toJSON {
                  options = opts;
                  podcasts = cfg.podcastList;
                };

              systemd.timers."podcast-horder" = {
                wantedBy = ["timers.target"];
                timerConfig = {
                  OnBootSec = "3m";
                  OnUnitActiveSec = cfg.checkFrequency;
                  Unit = "podcast-horder.service";
                };
              };

              systemd.services."podcast-horder" = {
                script = ''
                  ${self.packages."x86_64-linux".podcast-horder}/bin/podcast-horder
                '';
                serviceConfig = {
                  Type = "oneshot";
                  User = "root";
                };
              };
            };
          };
      };
    };
}
