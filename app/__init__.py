import getpodcast as get
import json

def openConfig(file: str) -> dict:
  with open(file) as podcastsFile:
    return json.load(podcastsFile)

def parsePodcastList(config: dict) -> dict:
    podcastList = dict()
    for item in config["podcasts"]:
      podcastList[item["title"]] = item["url"]
    return podcastList

def main() -> int:
  config = openConfig("/etc/podcast-horder/config.json")
  options = get.options(**config["options"])

  get.getpodcast(parsePodcastList(config), options)

if __name__ == '__main__':
    main()